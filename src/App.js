import React from 'react';
import './App.css';


import AssesmentConfig from './components/AssesmentConfig';

function App() {
  return (
    <div className="App" style={{padding: "10px", margin:"20px"}}>
      <AssesmentConfig />
    </div>
  );
}

export default App;
