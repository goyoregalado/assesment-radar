import React, {useState} from 'react';

import SvgComponent from './SvgComponent';


const AssesmentConfig = () => {
    const [skills, setSkills] = useState([]);
    const [numSkills, setNumSkills] = useState(4);

    const skillChangeHandler = (event) => {
        setNumSkills(parseInt(event.target.value));
    }

    return (
        <div className="assesmentConfig">
            <input type="range" min="3" max="8" value={numSkills} onChange={skillChangeHandler} />
            <SvgComponent radius="300" sides={numSkills} numMarks="5"/>
        </div>
    );

}

export default AssesmentConfig;