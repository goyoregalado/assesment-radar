import React from 'react';

import { calculateVertex, calculateEdges, calculateMainAxis, calculateSubEdges } from './SvgComponent';

test('calculateVertex returns the correct number of vertex', () => {
    const points = calculateVertex(4, 300, 150, 150);
    expect(points.length).toEqual(4);
});

test('calculateVertex return the correct coordinates for 4 points', () => {
    const points = calculateVertex(4, 300, 150, 150);

    expect(points[0].x).toBeCloseTo(450, 2);
    expect(points[0].y).toBeCloseTo(150, 2);
    expect(points[1].x).toBeCloseTo(150, 2);
    expect(points[1].y).toBeCloseTo(450, 2);
    expect(points[2].x).toBeCloseTo(-150, 2);
    expect(points[2].y).toBeCloseTo(150, 2);
    expect(points[3].x).toBeCloseTo(150, 2);
    expect(points[3].y).toBeCloseTo(-150, 2);
});

test('calculateEdges returns the correct number of edges', () => {
    const points = calculateVertex(4, 300, 150, 150);
    const edges = calculateEdges(points);
    expect(edges.props.children.length).toEqual(4);
});

test('calculateMainAxis returns two child elements', () => {
    const mainAxis = calculateMainAxis(300, 150, 150);
    expect(mainAxis.props.children.length).toEqual(2);
});

test('calculate subEdgesreturns the same number of subEdges than the number of possible marks', () => {
    const subAxis = calculateSubEdges(5, 5, 300, 150, 150);
    expect(subAxis.props.children.length).toEqual(5);
});