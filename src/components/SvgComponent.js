import React from 'react';

const FULLCIRCLE = 360.0;
const RADIAN_CONV_CONSTANT = Math.PI/180.0;



export const calculateVertex = (sides, radius, centerX, centerY) => {
    let angle = 0;
    const angleDelta = (FULLCIRCLE / sides) * RADIAN_CONV_CONSTANT;

    
    const output = [];
    const parsedRadius = parseInt(radius);

    for (let i = 0; i < sides; i++) {
        const sine = Math.sin(angle);
        const cosine = Math.cos(angle);
        const point = {x: 0, y: 0};
        point.x = parsedRadius * cosine + centerX;
        point.y = parsedRadius * sine + centerY;
        output.push(point);
        angle += angleDelta;
    }

    return output;
}

// We only use radius as key because it's unique.
export const calculateEdges = (points, radius=0) => {

    const lines = points.map((point, idx) => {
        let nextPoint = null;
        if (idx >= 0 && idx < points.length - 1) {
            nextPoint = points[ idx + 1] ;
        } else if ( idx === points.length - 1 ){
            nextPoint = points[0];
        }
        return (
            <line key={idx}  
                x1={point.x} 
                y1={point.y} 
                x2={nextPoint.x} 
                y2={nextPoint.y}
                
            />
        );
    });
    return (
        <g key={radius}>
        { lines }
        </g>
    );
};

export const calculateMainAxis = (radius, centerX, centerY) => (
    <>
        <g stroke="grey" strokeWidth="2" strokeDasharray="6 2" fill="black">
            <line 
                x1={centerX - radius}
                y1={centerY}
                x2={centerX + radius}
                y2={centerY}
            />
            <line 
                x1={centerX}
                y1={centerY - radius}
                x2={centerX}
                y2={centerY + radius}
            />
        </g>
        <circle cx={centerX} cy={centerY} r="5"/>

    </>
)

export const calculateSubEdges = (numMarks, sides, radius, centerX, centerY) => {

    const radiusDelta = radius / numMarks;
    let newRadius = radiusDelta;

    const subEdges = [];

    while (newRadius <= radius) {
        const points = calculateVertex(sides, newRadius, centerX, centerY);
        const aux = calculateEdges(points, newRadius);
        subEdges.push(aux);
        newRadius += radiusDelta;
    }

    return (
        <>
            { subEdges }
        </>
    );
}


const SvgComponent = ({radius, sides, numMarks}) => {

    const width = parseInt(radius) * 2;
    const height = parseInt(radius) * 2;

    // At this point we want the polygon centered.
    const centerX = parseInt(radius);
    const centerY = parseInt(radius);

    const clickHandler = (event) => {
        console.log("Lo que ha hecho...");
    }


    const points = calculateVertex(sides, radius, centerX, centerY);
    const edges = calculateEdges(points);
    const subEdges = calculateSubEdges(numMarks, sides, radius, centerX, centerY);
    const mainAxis = calculateMainAxis(radius, centerX, centerY);


    return (
        <svg width={`${width}px`} height={`${height}px`} viewBox={`0 0 ${width} ${height}`}>
            <rect width={width} height={height} stroke="blue" fill="none"/>
            <ellipse cx={300} cy={300} rx={radius} ry={radius} stroke="black" fill="none"/>
            
            <g stroke="black" strokeWidth="3">
                { edges }
            </g>

            <g stroke="grey" strokeWidth="2" strokeDasharray="6 2">
                { subEdges }
            </g>
            

            { mainAxis }
            
            { points.map((point, idx) => (
                <circle key={idx} cx={Math.ceil(point.x)} cy={Math.ceil(point.y)} r="8" fill="orange" onClick={clickHandler}/>
            ))}
            
        </svg>
    );
}



export default SvgComponent;